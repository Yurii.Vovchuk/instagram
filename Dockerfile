FROM node:15

WORKDIR /var/www

COPY ./package*.json ./
RUN npm install

COPY src/. ./src

CMD ["npm", "run", "start-server"]