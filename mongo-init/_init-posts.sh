#!/bin/bash
set -e;

# a default non-root role
MONGO_NON_ROOT_ROLE="${MONGO_NON_ROOT_ROLE:-readWrite}"

if [ -n "${MONGO_NON_ROOT_USERNAME:-}" ] && [ -n "${MONGO_NON_ROOT_PASSWORD:-}" ]; then
	"${mongo[@]}" "$MONGO_INITDB_DATABASE" <<-EOJS
		db.createUser({
			user: $(_js_escape "$MONGO_NON_ROOT_USERNAME"),
			pwd: $(_js_escape "$MONGO_NON_ROOT_PASSWORD"),
			roles: [ { role: $(_js_escape "$MONGO_NON_ROOT_ROLE"), db: $(_js_escape "$MONGO_INITDB_DATABASE") } ]
			})
	EOJS
  
  "${mongo[@]}" "$MONGO_INITDB_DATABASE" <<-EOJS1
    use $(_js_escape "$MONGO_NON_ROOT_USERNAME");
    db.auth($(_js_escape "$MONGO_NON_ROOT_USERNAME"), $(_js_escape "$MONGO_NON_ROOT_PASSWORD"));
    db.posts.insertMany([
			{
				photo: "/images/01.jpg",
				likes: 2391,
				description: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt",
				user: {
					name: "John",
					avatar: "/images/ava1.jpg"
				}
			},
			{
				photo: "/images/02.jpg",
				likes: 984,
				description: "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish",
				user: {
					name: "Bob",
					avatar: "/images/ava2.jpg"
				}
			},
			{
				photo: "/images/03.jpg",
				likes: 4,
				description: "Пять столетий спустя Lorem Ipsum испытал всплеск популярности с выпуском сухого переноса листов Letraset в. Эти листы надписи можно потереть на любом месте и были быстро приняты художники-графики, принтеры, архитекторов и рекламодателей для их профессионального вида и простоты использования.",
				user: {
					name: "Mary",
					avatar: "/images/ava3.jpg"
				}
			},
		]);
	EOJS1
else
	echo "Missing non root settings"
fi