import './App.scss';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import Header from './components/Header/Header'
import Posts from './components/Posts/Posts'
import Subscribers from './components/Subscribers/Subscribers'
import Userpage from './components/Userpage/Userpage';


function App(props) {
  const user = {
    ava: "https://www.blast.hk/attachments/64805/"
  }
  
  const names = [
    {name: 'neo', ava: '/images/ava1.jpg'},
    {name: 'john_rev', ava: '/images/ava2.jpg'},
    {name: 'mark_jonson', ava: '/images/ava3.jpg'},
    {name: 'dina_j', ava: '/images/ava4.jpg'},
  ]

  return (
    <Router>
      <div className="App">
        <Header avatar={user.ava}/>
        <Switch>
          <Route path='/' exact>
            <div className='app__container'>
            <Posts />
            <Subscribers 
              sub={names}
              avatar={user.ava}
            />
          </div>
          </Route>
          <Route path='/userpage'>
            <Userpage/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
