import React from 'react';
import './Avatar.scss';
import {Link} from 'react-router-dom';


const Avatar = (props) => {
  const {avatar, className} = props;
 
  return(
    <Link to='/userpage' className='navbar__link'>
      <img src={avatar} alt='avatar' className={`avatar ${className}`}></img>
    </Link>
  )
}

export default Avatar;