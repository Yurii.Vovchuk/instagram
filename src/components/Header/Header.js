import React from 'react';
import './Header.scss';
import Navbar from '../Navbar/Navbar';
import {Link} from 'react-router-dom';


const Header = (props) => {
const {avatar} = props;

  return( 
    <div className='header'>
      <div className='header__container'>
        <Link to='/'>
          <img src='logo.png' alt='logo' className='logo'></img>
        </Link>
        <div>
          <input type='text' placeholder='Search' className='header_input'></input>
        </div>
        <Navbar avatar={avatar}/>
      </div>
    </div>
  )
}

export default Header;