import React, {useState} from 'react';
import './Post.scss';
import Userbar from '../Userbar/Userbar';
import Postaction from '../Postaction/Postaction';
import {Link} from 'react-router-dom';


const Post = (props) => {

  const {photo, likes, description, user} = props.post;
  // const [liked, setLiked] = useState(false);
  
  // function doubleClickHandler() {
  //   setLiked(true);
  // }
  
  return(
    <div className='post__container'>
      <Userbar 
        avatar={user.avatar} 
        alt='ava'
        name={user.name}
      />

      <img  className='post__img' 
            src={photo}
            alt='piano'>
      </img>

      <div className='postaction__container'>
        <Postaction />
        <div>
          <p className='post__likesNumber'>{likes} вподобань</p>
          <p className='post__description'>
            <Link to='/userpage' className='description__username'>{user.name}</Link> Photo by @mattiasklumofficial / Sculpted natural works of art on the shore of the glacial lake Jökulsárlón near Vatnajökull National Park. This lake quite recently became the largest on Iceland since its glacial retreat extended its boundaries. I have many favourite places on Iceland and this is definitely one of them. Please follow @mattiasklumofficial for more of my images and films from around the world. #jökulsárlón #vatnajökull #landscape @alexandrovklumofficial
          </p>
          <p className='post__comment-counter'>Переглянути всі коментарі: 125</p>
          <p className='post__description'>{description}</p>
        </div>
      </div>
    </div>
  )
}

export default Post;
