import React, {useEffect, useState}  from 'react';
import Post from '../Post/Post';
import './Posts.scss';
import {getPosts} from '../../services/getData';

const Posts = () => {

  const [posts, setPosts] = useState([]);

  useEffect(() => {
    getPosts()
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      setPosts(data);
    });

  })

  

  return(
    <div className='posts__container' >
      {posts.map((item, index) => 
      <Post
        post={item}
        key={index}
      />)}
    </div>
  )
}

export default Posts;
