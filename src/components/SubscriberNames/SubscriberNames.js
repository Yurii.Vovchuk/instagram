import React from 'react';
import Avatar from '../Avatar/Avatar';
import './SubscriberNames.scss';
import {Link} from 'react-router-dom';


const SubscriberNames = (props) => {
  const {sub} = props;

  return(
    <div>
      {sub.map((item, index)=>
      <div className='sub__wrap'>
        <div className='sub__container'>
          <Avatar 
            className='sub__avatar'
            avatar={item.ava}
            key={index}
          />
          <div>
            <Link to='/userpage' className='sub__link'>
              {item.name}
            </Link>
            <div className='sub__status'>Стежить за вами</div>
          </div>
        </div>
        <div className='go-watch'>Стежити</div>
      </div>
      )}
    </div>
  )
}

export default SubscriberNames;