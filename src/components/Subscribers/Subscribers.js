import React from 'react';
import './Subscribers.scss';
import Avatar from '../Avatar/Avatar';
import SubscriberNames from '../SubscriberNames/SubscriberNames';
import {Link} from 'react-router-dom';



const Subscribers = (props) => {
  const {avatar, sub} = props;

  const user = {name: 'yurii_vovchuk'};
 
  return(
   <div className='subscriber__container'>
     <div className='subscriber__block'>
       <div className='avatar__container'>
        <div className='avatar__wrap'>
          <Avatar 
            className='subscriber__avatar'
            avatar={avatar}
            />
          <p className='subscriber__user-name'>{user.name}</p>
        </div>
        <Link to='/userpage' className='go-watch__link'> <div className='go-watch'>Перейти</div></Link>
      </div>
      </div>
      <div className='subscribers__wrap'>
        <div className='subscribers__title'>Рекомендації для вас</div>
        <div className='go-watch__title'>Переглянути всіх</div>
      </div>
      <div>
        <SubscriberNames 
          sub={sub}
       />
       </div>
   </div>
  )
}

export default Subscribers;