import React from 'react';
import './Userbar.scss';
import Avatar from '../Avatar/Avatar';
import {Link} from 'react-router-dom';


const Userbar = (props) => {

const {avatar, alt, name} = props;

  return(
    <div className='userbar__container'>
      <Link to='/userpage' className='userbar__link'>
        <Avatar 
          className='userbar__avatar' 
          avatar={avatar}
        />
        
        <div className='userbar__name'>{name}</div>
      </Link>
      <Link to='/userpage' className='userbar-menu__link'>
        <img src='ellipsis-h-solid.svg' alt='menu' className='userbar-menu__img'></img>
      </Link>
    </div>
  )
}

export default Userbar;
