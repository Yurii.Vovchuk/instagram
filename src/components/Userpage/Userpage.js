import React, {useState} from 'react';
import './Userpage.scss';

const Userpage = () => {

  const [liked, setLiked] = useState(false);

function DoubleClickHandler() {
  setLiked(true)
}

const Liked = 'Liked';
const DisLiked = 'DisLiked';

  return(
    <div className='userpage__container'>
      {liked ? 'Liked' : ''}
      <img 
        onDoubleClick={DoubleClickHandler}
        className='userpage__img'
        src='https://www.opendesignsin.com/blog/wp-content/uploads/2020/11/under-construction.jpg' 
        alt='Page under construction'>
      </img>
    </div>
  )
}


export default Userpage;