const express = require('express');
const app = express();
const mongoose = require('mongoose');
mongoose.connect(
  'mongodb://mongo:27017/instagram',
  {
    useNewUrlParser: true,
    user: "instagram",
    pass: "instagram-secret",
    dbName: "instagram",
    useCreateIndex: true,
    useUnifiedTopology: true
  }
);

const postSchema = new mongoose.Schema({
  photo: String
}, {collection: "posts"});

const Post = mongoose.model('Post', postSchema);

app.get("/posts", function(req, res) {
  Post.find().exec(function(error, results) {
    res.set('Access-Control-Allow-Origin', '*');
    res.send((results));
  });
});

app.listen(3333);